package com.vinit.flickrslide.di

import android.arch.lifecycle.ViewModel
import com.vinit.flickrslide.di.component.DaggerViewModelInjector
import com.vinit.flickrslide.di.component.ViewModelInjector
import com.vinit.flickrslide.di.module.NetworkModule
import com.vinit.flickrslide.viewmodel.PhotoListViewModel

abstract class BaseViewModel:ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is PhotoListViewModel -> injector.inject(this)
        }
    }
}