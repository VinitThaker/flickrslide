package com.vinit.flickrslide.data.model

data class FlickrApiResponse(
        val photos: Photos,
        val stat: String
)