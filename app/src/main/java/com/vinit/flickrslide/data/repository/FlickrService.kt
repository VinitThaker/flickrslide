package com.vinit.flickrslide.data.repository

import com.vinit.flickrslide.data.model.FlickrApiResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface FlickrService {

    @GET("services/rest/")
    fun getPhotosList(@Query("method") method: String,
                      @Query("api_key") apiKey: String,
                      @Query("extras") extras: String,
                      @Query("format") format:String,
                      @Query("nojsoncallback") callback : Int): Observable<Response<FlickrApiResponse>>
}