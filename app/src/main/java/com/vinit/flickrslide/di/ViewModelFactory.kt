package com.vinit.flickrslide.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.v4.app.FragmentActivity
import com.vinit.flickrslide.util.imageutil.PhotoLoader
import com.vinit.flickrslide.viewmodel.PhotoListViewModel

class ViewModelFactory(private val activity: FragmentActivity): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PhotoListViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return PhotoListViewModel(activity.application, activity.windowManager, PhotoLoader.getInstance(activity)) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}