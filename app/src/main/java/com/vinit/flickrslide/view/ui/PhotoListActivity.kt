package com.vinit.flickrslide.view.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.vinit.flickrslide.R

class PhotoListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_list)

        // Add project list fragment if this is first creation
        if (savedInstanceState == null) {
            val fragment = PhotoListFragment()

            supportFragmentManager.beginTransaction()
                    .add(R.id.fragment_container, fragment, TAG).commit()
        }
    }
}
