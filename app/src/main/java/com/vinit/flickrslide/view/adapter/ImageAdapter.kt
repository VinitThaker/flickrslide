package com.vinit.flickrslide.view.adapter

import android.content.Context
import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.vinit.flickrslide.R
import kotlinx.android.synthetic.main.photo_item_layout.view.*
import java.util.*

internal class ImageAdapter(private val context: Context) : RecyclerView.Adapter<ImageAdapter.PhotoViewHolder>() {
    private var imageEntities: List<Bitmap>
    private val requestOptions: RequestOptions

    fun changeData(imageEntities: List<Bitmap>) {
        this.imageEntities = imageEntities
        notifyDataSetChanged()
    }

    init {
        imageEntities = ArrayList()
        requestOptions = RequestOptions()
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val itemLayoutView = LayoutInflater.from(parent.context).inflate(R.layout.photo_item_layout, parent, false)
        return PhotoViewHolder(itemLayoutView)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val imageEntity = imageEntities[position]
        Glide.with(context).load(imageEntity).apply(requestOptions).into(holder.itemView.image_view!!)
    }

    override fun getItemCount(): Int {
        return imageEntities.size
    }

    internal class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}