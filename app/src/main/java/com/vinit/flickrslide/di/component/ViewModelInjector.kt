package com.vinit.flickrslide.di.component

import com.vinit.flickrslide.di.module.NetworkModule
import com.vinit.flickrslide.viewmodel.PhotoListViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified PhotoListViewModel.
     * @param photoListViewModel PhotoListViewModel in which to inject the dependencies
     */
    fun inject(photoListViewModel: PhotoListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}