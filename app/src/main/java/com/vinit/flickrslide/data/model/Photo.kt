package com.vinit.flickrslide.data.model

data class Photo(
        val id: String,
        val owner: String,
        val secret: String,
        val server: String,
        val farm: Int,
        val title: String,
        val ispublic: Int,
        val isfriend: Int,
        val isfamily: Int,
        val url_n: String,
        val height_n: Int,
        val width_n: Int
)