package com.vinit.flickrslide.util.networkutil;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.ResponseBody;

public class NetworkUtils {
    public static RestError handleError(ResponseBody response) {
        RestError restError = new RestError();
        restError.setStatus(400);
        restError.setMessage("Something Went Wrong!");
        if (response != null) {

            try {
                BasicResponse basicResponse = (new Gson()).fromJson(new String(response.bytes()), BasicResponse.class);
                restError = basicResponse.getError();
            } catch (IOException var4) {
                var4.printStackTrace();
            }
        }

        return restError;
    }

}
