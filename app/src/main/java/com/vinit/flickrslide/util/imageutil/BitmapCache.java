package com.vinit.flickrslide.util.imageutil;

import android.graphics.Bitmap;

/**
 * Interface for a fixed-size local storage.
 * Implemented by {@link MemoryBitmapCache} and {@link DiskBitmapCache}.
 */
public interface BitmapCache {

    /**
     * For debugging
     */
    String getName();

    /**
     * Whether any object with key exists
     */
    boolean containsKey(String key);

    /**
     * Gets the object mapped against key
     */
    Bitmap get(String key);

    /**
     * Saves bitmapToSave against key
     */
    void save(String key, Bitmap bitmapToSave);

    /**
     * Deletes everything in this cache
     */
    void clear();

}