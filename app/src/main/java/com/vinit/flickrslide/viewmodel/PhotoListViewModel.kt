package com.vinit.flickrslide.viewmodel

import android.app.Application
import android.arch.lifecycle.*
import android.graphics.Bitmap
import android.support.v4.util.SparseArrayCompat
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowManager
import com.vinit.flickrslide.BuildConfig
import com.vinit.flickrslide.util.imageutil.PhotoLoader
import com.vinit.flickrslide.data.model.FlickrApiResponse
import com.vinit.flickrslide.data.model.Photo
import com.vinit.flickrslide.data.repository.FlickrService
import com.vinit.flickrslide.di.BaseViewModel
import com.vinit.flickrslide.util.networkutil.NetworkUtils
import retrofit2.Response
import rx.Observable
import rx.Observer
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.ArrayList
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class PhotoListViewModel(application: Application, windowManager: WindowManager, photoLoader: PhotoLoader) : BaseViewModel() {

    @Inject
    lateinit var flickrService: FlickrService

    private var mPhotoLoader: PhotoLoader = photoLoader
    private var mWindowManager: WindowManager = windowManager

    var photoListObservable: MutableLiveData<List<Bitmap>> = MutableLiveData()
    var loadingObserver: MutableLiveData<Boolean> = MutableLiveData()
    var errorMessage: MutableLiveData<String> = MutableLiveData()

    private lateinit var subscription: Subscription
    private lateinit var batches: SparseArrayCompat<List<Photo>>

    val FLICKR_METHOD = "flickr.interestingness.getList"
    val FLICKR_EXTRA = "url_n"
    val FLICKR_FORMAT = "json"
    val FLICKR_JSON_CALLBACK = 1

    private var deviceHeight: Int
    private var deviceWidth: Int
    private var remainingHeight: Int = 0

    init {
        val displayMetrics = DisplayMetrics()
        mWindowManager.defaultDisplay.getMetrics(displayMetrics)
        deviceHeight = displayMetrics.heightPixels
        deviceWidth = displayMetrics.widthPixels

        loadImages()
    }

    private fun loadImages() {

        subscription = flickrService.getPhotosList(FLICKR_METHOD, BuildConfig.API_KEY, FLICKR_EXTRA, FLICKR_FORMAT, FLICKR_JSON_CALLBACK)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object: Observer<Response<FlickrApiResponse>> {
                    override fun onError(e: Throwable) {
                        errorMessage.postValue(e.message)
                    }

                    override fun onNext(flickrApiResponse: Response<FlickrApiResponse>) {
                        if (flickrApiResponse.isSuccessful) {
                            if (flickrApiResponse.body()!=null) {
                                createBatches(flickrApiResponse.body().photos.photo)
                            }
                        }else{
                            val restError = NetworkUtils.handleError(flickrApiResponse.errorBody())
                            errorMessage.postValue(restError.message)
                        }
                    }

                    override fun onCompleted() {}

                })
    }

    override fun onCleared() {
        super.onCleared()
        subscription.unsubscribe()
    }

    private fun createBatches(imageList: List<Photo>) {
        batches = SparseArrayCompat() //SparseArrayCompat is better than HashMap for size < 1000
        remainingHeight = deviceHeight
        var index = 0
        val sameBatchPhotos = ArrayList<Photo>()
        for (photo in imageList) {
            if (photo.height_n < deviceHeight && photo.width_n < deviceWidth) {
                if (photo.height_n < remainingHeight) {
                    sameBatchPhotos.add(photo)
                    remainingHeight -= photo.height_n
                } else {
                    val photoList = ArrayList<Photo>()
                    photoList.addAll(sameBatchPhotos)
                    batches.append(index, photoList) //append() better for performance when key is always greater than other keys
                    index++ //increment batch counter
                    sameBatchPhotos.clear()
                    remainingHeight = deviceHeight
                }
            }
        }
        downloadAndLoadBatch(0)
        initPhotoBatchStream()
    }

    private fun downloadAndLoadBatch(i: Int) {
        loadingObserver.postValue(true)
        Observable.from(batches.get(i))
                /**
                 * PhotoLoader Loads a photo from one of these 3 sources:
                 * 1. Memory cache — if it was downloaded in the current session
                 * 2. Disk cache   — if it was ever downloaded
                 * 3. Network      — if it was never downloaded
                 */
                .flatMap<Bitmap> { photo -> mPhotoLoader.load(photo.url_n) }
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : rx.Observer<List<Bitmap>> {
                    override fun onCompleted() {}

                    override fun onError(e: Throwable) {
                        loadingObserver.postValue(false)
                        errorMessage.postValue(e.message)
                    }

                    override fun onNext(photos: List<Bitmap>) {
                        loadingObserver.postValue(false)
                        photoListObservable.value = photos
                    }
                })
    }

    private fun initPhotoBatchStream() {
        Observable.range(0, batches.size())
                .skip(1) //since we are loading first batch directly after creating batches, we are skipping it here
                .zipWith(Observable.interval(10, TimeUnit.SECONDS)) { index, _ -> index } //it will emit item after every 10 second
                .doOnNext{this.downloadAndLoadBatch(it)}
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : rx.Observer<Int> {
                    override fun onCompleted() {}

                    override fun onError(e: Throwable) {
                        errorMessage.postValue(e.message)
                    }

                    override fun onNext(index: Int) {
                        Log.d("load", "batch $index")
                    }
                })
    }

}