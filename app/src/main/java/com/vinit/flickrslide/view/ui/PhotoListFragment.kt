package com.vinit.flickrslide.view.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.vinit.flickrslide.R
import com.vinit.flickrslide.di.ViewModelFactory
import com.vinit.flickrslide.view.adapter.ImageAdapter
import com.vinit.flickrslide.viewmodel.PhotoListViewModel
import kotlinx.android.synthetic.main.fragment_photo_list.*

const val TAG = "PhotoListFragment"
class PhotoListFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_photo_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val staggeredGridLayoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        recycler_view.layoutManager = staggeredGridLayoutManager

        val imageAdapter = ImageAdapter(context!!)
        recycler_view.adapter = imageAdapter

        val viewModel = ViewModelProviders.of(this, ViewModelFactory(activity!!)).get(PhotoListViewModel::class.java)

        /**
         * Since we are using LiveData with lifecycle owner, we don't have to handle the lifecycle of the observer.
         * If the owner moves to the {Lifecycle.State#DESTROYED} state, the observer will
         * automatically be removed.
         */
        viewModel.photoListObservable.observe(this, Observer { photos ->
            if (photos != null) {
                imageAdapter.changeData(photos)
            }
        })

        /**
         * In MVVM, view should be as light as possible. It should only listen to observables provided by view model and react to the change.
         */
        viewModel.loadingObserver.observe(this, Observer { showLoading ->
            progress_bar.visibility = if (showLoading!!) View.VISIBLE else View.GONE
        })

        viewModel.errorMessage.observe(this, Observer { errorMessage ->
           Toast.makeText(context,errorMessage,Toast.LENGTH_SHORT).show()
        })
    }

}
